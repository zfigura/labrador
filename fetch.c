/*
 * Copyright 2024 Elizabeth Figura
 *
 * This file is part of Labrador.
 *
 * Labrador is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Labrador is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Labrador; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <curl/curl.h>
#include <json_object.h>
#include <json_tokener.h>
#include <json_util.h>
#ifdef HAVE_LIBGIT2
#include <git2/buffer.h>
#include <git2/commit.h>
#include <git2/diff.h>
#include <git2/errors.h>
#include <git2/global.h>
#include <git2/oid.h>
#include <git2/remote.h>
#include <git2/repository.h>
#include <git2/revwalk.h>
#include <git2/tree.h>
#endif

#include "labrador.h"

/* todo: config */
static const bool newest_top = false;

static void die(const char *format, ...)
{
    va_list args;

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    exit(1);
}

static void log_time(void)
{
    struct timespec ts;
    struct tm tm;
    time_t t;

    clock_gettime(CLOCK_REALTIME, &ts);
    t = ts.tv_sec;
    localtime_r(&t, &tm);
    fprintf(stderr, "[%02u:%02u:%02u.%03u] ", tm.tm_hour, tm.tm_min, tm.tm_sec,
            (unsigned int)(ts.tv_nsec / 1000000));
}

static void info(const char *format, ...)
{
    va_list args;

    log_time();
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
}

static void err(const char *format, ...)
{
    va_list args;

    log_time();
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
}

static bool array_reserve(void **elements, size_t *capacity, size_t count, size_t size)
{
    unsigned int new_capacity, max_capacity;
    void *new_elements;

    if (count <= *capacity)
        return true;

    max_capacity = ~(size_t)0 / size;
    if (count > max_capacity)
        return false;

    new_capacity = *capacity;
    if (new_capacity < 8)
        new_capacity = 8;
    while (new_capacity < count && new_capacity <= max_capacity / 2)
        new_capacity *= 2;
    if (new_capacity < count)
        new_capacity = max_capacity;

    if (!(new_elements = realloc(*elements, new_capacity * size)))
        return false;

    *elements = new_elements;
    *capacity = new_capacity;

    return true;
}

char *format_date(uint64_t timestamp, size_t *ret_size)
{
    size_t size = 100;
    char *ret = malloc(size);
    time_t t = timestamp;
    struct tm tm;

    localtime_r(&t, &tm);
    while (!(*ret_size = strftime(ret, size, "%c", &tm)))
    {
        size *= 2;
        ret = realloc(ret, size);
    }
    return ret;
}

static bool parse_time(const char *string, size_t len, uint64_t *timestamp)
{
    struct tm tm = {0};
    const char *ret;

    /* Gitlab dates are documented as being in ISO 8601.
     * In practice they (fortunately) seem to be a very specific format.
     * Parse the specific format until we find any evidence we need to do
     * otherwise. */

    ret = strptime(string, "%Y-%m-%dT%H:%M:%S", &tm);
    if (!ret)
    {
        err("Unexpected date format \"%s\".\n", string);
        return false;
    }
    /* Skip milliseconds. */
    if (*ret == '.')
    {
        ++ret;
        while (*ret >= '0' && *ret <= '9')
            ++ret;
    }
    if (strcmp(ret, "Z"))
    {
        err("Unexpected date format \"%s\".\n", string);
        return false;
    }

    *timestamp = mktime(&tm);
    return true;
}

/* Please don't copy data between a big-endian and little-endian machine. */

#define FLAG_UNREAD 0x1
/* Internal only. */
#define FLAG_UPDATED 0x8000

struct buried_mr
{
    uint32_t mrid;
    uint32_t user_index;
    uint16_t flags;
    uint16_t title_len;
    uint16_t rev_count;
    uint16_t comment_count;
    uint32_t title_offset;
    uint32_t desc_len;
    uint32_t desc_offset;
    uint32_t revs_offset;
    uint32_t comments_offset;
};

struct buried_rev
{
    uint64_t time;
    uint16_t flags;
    uint16_t patch_count;
    uint32_t patches_offset;
};

struct chewed_comment
{
    struct buried_comment buried;
    bool new;
    bool is_patch_comment;
    unsigned int old_line;
    unsigned int new_line;
    const char *sha;
    const char *old_path;
    const char *new_path;
};

struct buried_author
{
    uint16_t name_len;
    uint16_t email_len;
    uint32_t offset;
};

struct project
{
    struct buried_mr *mrs;
    struct buried_rev *revs;
    struct buried_patch *patches;
    struct buried_comment *comments;
    struct buried_reply *replies;
    struct buried_author *authors;
    struct buried_user *users;
    size_t mr_count, rev_count, patch_count, comment_count, reply_count, author_count, user_count;

    FILE *strings_file;

    const char *local_name;
    const char *display_name;
    const char *token;
    const char *hostname;
    const char *project_path;
    /* FIXME: Shouldn't need the user to specify project id. */
    const char *project_id;

#ifdef HAVE_LIBGIT2
    git_repository *repository;
    git_remote *remote;
#endif

    struct
    {
        CURL *curl;
        FILE *mrs_file, *revs_file, *patches_file, *comments_file, *replies_file, *strings_file;

        struct buried_author *authors;
        size_t author_count, authors_capacity;
        struct buried_user *users;
        size_t user_count, users_capacity;
    } writing;
};

size_t project_count = 0;
static struct project *projects;
static struct project *active_project;

static char *get_data_path(const struct project *project, const char *filename)
{
    char *home = getenv("XDG_DATA_HOME");
    char *ret;

    /* Technically we're supposed to respect XDG_DATA_DIRS too, but I'm not
     * sure it makes sense for us? This data is going to be rewritten into
     * $XDG_DATA_HOME every time the program runs. */

    if (home)
    {
        asprintf(&ret, "%s/labrador/%s/%s", home, project->local_name, filename);
    }
    else
    {
        home = getenv("HOME");
        asprintf(&ret, "%s/.local/share/labrador/%s/%s", home, project->local_name, filename);
    }

    return ret;
}

static void mkdir_data_path(const struct project *project)
{
    char *home = getenv("XDG_DATA_HOME");
    char *dir;

    if (home)
    {
        asprintf(&dir, "%s/labrador", home);
        if (mkdir(dir, 0755) < 0 && errno != EEXIST)
            die("Failed to create %s: %s\n", dir, strerror(errno));
        free(dir);
        asprintf(&dir, "%s/labrador/%s", home, project->local_name);
        if (mkdir(dir, 0755) < 0 && errno != EEXIST)
            die("Failed to create %s: %s\n", dir, strerror(errno));
        free(dir);
    }
    else
    {
        home = getenv("HOME");

        asprintf(&dir, "%s/.local/share/labrador", home);
        if (mkdir(dir, 0755) < 0 && errno != EEXIST)
            die("Failed to create %s: %s\n", dir, strerror(errno));
        free(dir);
        asprintf(&dir, "%s/.local/share/labrador/%s", home, project->local_name);
        if (mkdir(dir, 0755) < 0 && errno != EEXIST)
            die("Failed to create %s: %s\n", dir, strerror(errno));
        free(dir);
    }
}

static FILE *fopen_data_path(const struct project *project, const char *filename, const char *mode)
{
    char *path = get_data_path(project, filename);
    FILE *ret = fopen(path, mode);
    free(path);
    return ret;
}

static void initialize_project(struct project *project, const char *git_path)
{
    if (!project->token)
        die("No token found for [%s].\n", project->local_name);
    if (!project->hostname)
        die("No hostname found for [%s].\n", project->local_name);
    if (!project->project_path)
        die("No project found for [%s].\n", project->local_name);
    if (!project->project_id)
        die("No project_id found for [%s].\n", project->local_name);
    if (!project->display_name)
        project->display_name = strdup(project->local_name);

    mkdir_data_path(project);

    /* Don't open the strings file yet; it might be empty.
     * Wait until we actually have a string to read. */

#ifdef HAVE_LIBGIT2
    project->repository = NULL;

    if (git_path)
    {
        int ret;

        git_libgit2_init();

        if (!(ret = git_repository_open(&project->repository, git_path)))
        {
            char *remote_url;

            asprintf(&remote_url, "%s/%s.git", project->hostname, project->project_path);

            if (git_remote_create_anonymous(&project->remote, project->repository, remote_url))
                err("Failed to open remote \"%s\", error %d.\n", remote_url, ret);
        }
        else
        {
            err("Failed to open Git repository at \"%s\", error %d.\n", git_path, ret);
        }
    }
#endif
}

static void init_config(void)
{
    char *home = getenv("XDG_DATA_HOME");
    char *path, *git_path = NULL;
    char line[512];
    FILE *f;

    /* Again, we're supposed to be respecting XDG_CONFIG_DIRS, but we'll be
     * (eventually) writing this file too, so I don't think it makes sense? */

    if (home)
    {
        asprintf(&path, "%s/labrador.conf", home);
    }
    else
    {
        home = getenv("HOME");
        asprintf(&path, "%s/.config/labrador.conf", home);
    }

    if (!(f = fopen(path, "r")))
    {
        err("Failed to open %s: %s\n", path, strerror(errno));
        exit(1);
    }

    while (fgets(line, sizeof(line), f))
    {
        size_t key_len, value_len;
        const char *key, *value;
        const char *p = line;

        /* strip the newline in case we print an error message */
        if (strlen(line) && line[strlen(line) - 1] == '\n')
            line[strlen(line) - 1] = 0;

        while (isspace(*p))
            ++p;
        if (*p == '#' || *p == 0)
            continue;

        if (*p == '[')
        {
            const char *end = p;

            while (*end != ']')
            {
                if (*end == '/' || *end == '\0')
                    die("Illegal project name \"%s\".\n", p);
                ++end;
            }

            if (active_project)
                initialize_project(active_project, git_path);

            projects = realloc(projects, (project_count + 1) * sizeof(*projects));
            active_project = &projects[project_count++];
            memset(active_project, 0, sizeof(*active_project));

            active_project->local_name = strndup(p + 1, end - (p + 1));
            continue;
        }
        else if (!active_project)
        {
            err("Ignoring line not part of a section: \"%s\"\n", line);
            continue;
        }

        key = p;
        while (isalnum(*p) || *p == '_')
            ++p;
        key_len = p - key;
        if (!key_len)
        {
            err("Ignoring malformed line in config file: \"%s\"\n", line);
            continue;
        }

        while (isspace(*p))
            ++p;
        if (*p != '=')
        {
            err("Ignoring malformed line in config file: \"%s\"\n", line);
            continue;
        }
        ++p;
        while (isspace(*p))
            ++p;
        value = p;
        p = line + strlen(line);
        while (p > line && isspace(p[-1]))
            --p;
        value_len = p - value;

        if (!strncasecmp(key, "token", key_len))
            active_project->token = strndup(value, value_len);
        else if (!strncasecmp(key, "hostname", key_len))
            active_project->hostname = strndup(value, value_len);
        else if (!strncasecmp(key, "project", key_len))
            active_project->project_path = strndup(value, value_len);
        else if (!strncasecmp(key, "project_id", key_len))
            active_project->project_id = strndup(value, value_len);
        else if (!strncasecmp(key, "git_path", key_len))
            git_path = strndup(value, value_len);
        else if (!strncasecmp(key, "name", key_len))
            active_project->display_name = strndup(value, value_len);
        else
            err("Ignoring unrecognized key in config file: \"%s\"\n", line);
    }

    if (active_project)
        initialize_project(active_project, git_path);
}

/* This will eventually get more complicated :-)
 * We want to do some caching, and on-disk compression, and splitting
 * strings across multiple files... */
static char *dig_string_from(struct project *project, size_t offset, size_t len)
{
    size_t retlen;
    char *ret;

    if (!project->strings_file)
    {
        if (!(project->strings_file = fopen_data_path(project, "strings", "rb")))
            die("Failed to open strings file: %s\n", strerror(errno));
    }

    ret = malloc(len);
    fseek(project->strings_file, offset, SEEK_SET);
    if ((retlen = fread(ret, 1, len, project->strings_file)) != len)
    {
        err("Got %zu bytes reading string at offset %zu, length %zu: %s\n", retlen, offset, len, strerror(errno));
        exit(1);
    }
    return ret;
}

char *dig_string(size_t offset, size_t len)
{
    return dig_string_from(active_project, offset, len);
}

static void *dig_db(struct project *project, const char *name, size_t item_size, size_t *count)
{
    char *path = get_data_path(project, name);
    int fd = open(path, O_RDONLY | O_CLOEXEC);
    struct stat st;
    void *ret;

    if (fd == -1)
    {
        if (errno != ENOENT)
        {
            err("Failed to open %s: %s\n", path, strerror(errno));
            exit(1);
        }
        *count = 0;
        return NULL;
    }

    free(path);
    fstat(fd, &st);
    ret = malloc(st.st_size);
    read(fd, ret, st.st_size);
    close(fd);
    *count = st.st_size / item_size;
    return ret;
}

static bool bury_db(struct project *project, const char *name, const void *data, size_t size, size_t count)
{
    size_t ret;
    FILE *file;

    if (!(file = fopen_data_path(project, name, "wb")))
    {
        err("Failed to open \"%s\": %s\n", name, strerror(errno));
        return false;
    }
    if ((ret = fwrite(data, size, count, file)) != count)
    {
        err("Failed to write \"%s\": %s\n", name, strerror(errno));
        fclose(file);
        return false;
    }
    fclose(file);
    return true;
}

static void dig_mrs(struct project *project)
{
    project->mrs = dig_db(project, "mrs0", sizeof(struct buried_mr), &project->mr_count);
    project->revs = dig_db(project, "revs0", sizeof(struct buried_rev), &project->rev_count);
    project->patches = dig_db(project, "patches0", sizeof(struct buried_patch), &project->patch_count);
    project->comments = dig_db(project, "comments0", sizeof(struct buried_comment), &project->comment_count);
    project->replies = dig_db(project, "replies0", sizeof(struct buried_reply), &project->reply_count);
    project->authors = dig_db(project, "authors0", sizeof(struct buried_author), &project->author_count);
    project->users = dig_db(project, "users0", sizeof(struct buried_user), &project->user_count);

    for (size_t i = 0; i < project->mr_count; ++i)
    {
        const struct buried_mr *mr = &project->mrs[i];

        info("Dug up mr %u.\n", mr->mrid);

        if (mr->revs_offset + mr->rev_count > project->rev_count)
        {
            err("Error reading data: rev offset %u / count %u exceeds total count %zu.\n",
                    mr->revs_offset, mr->rev_count, project->rev_count);
            exit(1);
        }

        for (size_t j = 0; j < project->mrs[i].rev_count; ++j)
        {
            const struct buried_rev *rev = &project->revs[project->mrs[i].revs_offset + j];

            if (rev->patches_offset + rev->patch_count > project->patch_count)
            {
                err("Error reading data: patch offset %u / count %u exceeds total count %zu.\n",
                        rev->patches_offset, rev->patch_count, project->patch_count);
                exit(1);
            }
        }
    }
}

static struct json_object *get_json_value(struct json_object *parent, const char *key)
{
    struct json_object *object;

    if (!json_object_object_get_ex(parent, key, &object))
    {
        err("No \"%s\" element found.\n", key);
        return NULL;
    }
    return object;
}

static bool validate_json_array(struct json_object *obj)
{
    enum json_type type = json_object_get_type(obj);

    if (type == json_type_array)
        return true;

    err("Element has wrong type %s.\n", json_type_to_name(type));
    return false;
}

struct write_json_ctx
{
    struct json_tokener *tokener;
    struct json_object *object;
};

static size_t write_json_cb(char *ptr, size_t size, size_t count, void *context)
{
    struct write_json_ctx *ctx = context;
    size_t len = size * count;

    if (ctx->object)
    {
        err("Unexpected %zu bytes extra data after an object has already been read!\n", len);
        return 0;
    }

    // fixme zf: remove this eventually
    fwrite(ptr, size, count, stdout);

    ctx->object = json_tokener_parse_ex(ctx->tokener, ptr, len);
    if (!ctx->object && json_tokener_get_error(ctx->tokener) != json_tokener_continue)
    {
        err("Failed to parse.\n");
        return 0;
    }

    return len;
}

static struct json_object *do_query(struct project *project, const char *url)
{
    /* type safety paranoia...*/
    curl_write_callback cb = write_json_cb;
    CURL *curl = project->writing.curl;
    struct curl_slist *headers = NULL;
    char error[CURL_ERROR_SIZE];
    struct json_object *errors;
    struct write_json_ctx ctx;
    char *auth_header;
    CURLcode ret;

    info("Sending query to %s ...\n", url);

    ctx.tokener = json_tokener_new();
    ctx.object = NULL;

    asprintf(&auth_header, "Authorization: Bearer %s", project->token);
    headers = curl_slist_append(headers, auth_header);
    headers = curl_slist_append(headers, "Content-Type: application/json");

    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, error);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1l);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cb);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ctx);

    ret = curl_easy_perform(curl);
    curl_slist_free_all(headers);
    free(auth_header);

    fprintf(stdout, "\n");

    if (ret != CURLE_OK)
    {
        err("Curl failed, error %u: %s\n", ret, error);
        json_tokener_free(ctx.tokener);
        return NULL;
    }

    info("Query finished and parsed.\n");

    if (json_tokener_get_error(ctx.tokener) != json_tokener_success)
    {
        err("Got partial JSON string!\n");
        json_tokener_free(ctx.tokener);
        return NULL;
    }

    json_tokener_free(ctx.tokener);

    if (ctx.object && json_object_object_get_ex(ctx.object, "errors", &errors))
    {
        if (validate_json_array(errors))
        {
            for (size_t i = 0; i < json_object_array_length(errors); ++i)
                err("Got query error: %s\n", json_object_get_string(json_object_array_get_idx(errors, i)));
        }

        json_object_put(ctx.object);
        return NULL;
    }

    return ctx.object;
}

static size_t fwrite_cb(char *ptr, size_t size, size_t count, void *context)
{
    return fwrite(ptr, size, count, context);
}

static bool fetch_diff(CURL *curl, const char *url, FILE *strings_file, uint32_t *ret_len)
{
    /* type safety paranoia...*/
    curl_write_callback cb = fwrite_cb;
    char error[CURL_ERROR_SIZE];
    size_t offset;
    CURLcode ret;

    info("Fetching diff %s ...\n", url);

    offset = ftell(strings_file);

    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, error);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1l);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cb);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, strings_file);

    if ((ret = curl_easy_perform(curl)) != CURLE_OK)
    {
        err("Curl failed, error %u: %s\n", ret, error);
        /* Truncate the strings file back to the previous offset.
         * There's no streams API for this, so we have to flush libc's internal
         * buffering and then use ftruncate(). */
        fflush(strings_file);
        fseek(strings_file, offset, SEEK_SET);
        ftruncate(fileno(strings_file), offset);
        return false;
    }

    info("done.\n");

    *ret_len = ftell(strings_file) - offset;
    return true;
}

static struct json_object *do_graphql_query(struct project *project, const char *req, size_t req_len)
{
    struct json_object *object;
    char *url;

    curl_easy_setopt(project->writing.curl, CURLOPT_POSTFIELDS, req);
    curl_easy_setopt(project->writing.curl, CURLOPT_POSTFIELDSIZE, req_len);

    asprintf(&url, "%s/api/graphql", project->hostname);
    object = do_query(project, url);
    free(url);
    return object;
}

/* As late as December 2023, GitLab's documentation on GraphQL included the
 * following statement [1]:
 *
 *   We want the GraphQL API to be the *primary* means of interacting
 *   programmatically with GitLab. To achieve this, it needs full coverage -
 *   anything possible in the REST API should also be possible in the GraphQL
 *   API.
 *
 *   To help us meet this vision, the frontend should use GraphQL in preference
 *   to the REST API for new features.
 *
 * After seeing this, and going to the trouble of learning GraphQL, I have now
 * found no less than three different things that can be done with REST and not
 * GraphQL:
 *
 *   - creating cross-project merge requests
 *   - listing merge request revisions
 *   - listing merge request open/close/merge events (with timestamp and actor)
 *
 * Hilariously, and probably not coincidentally, the relevant paragraphs have
 * been removed since then.
 *
 * We use GraphQL where possible anyway, mostly because it allows batching
 * together different requests to some degree (i.e. we can gather merge
 * requests and comments at the same time) and because we can reduce the amount
 * of data sent over the wire by being selective. But otherwise we use REST.
 *
 * [1] https://web.archive.org/web/20231208205447/https://docs.gitlab.com/ee/api/graphql/
 */
static struct json_object *do_rest_get(struct project *project, const char *url)
{
    curl_easy_setopt(project->writing.curl, CURLOPT_HTTPGET, 1l);
    return do_query(project, url);
}

static bool bury_string(struct project *project, const char *string, size_t len, uint32_t *offset)
{
    size_t ret;

    /* FIXME ZF: 4 GB strings offset is definitely too small... though maybe
     * just for patch contents? For everything else it's more reasonable. */

    *offset = ftell(project->writing.strings_file);
    if ((ret = fwrite(string, 1, len, project->writing.strings_file)) != len)
    {
        err("Failed to write %zu bytes to strings file: %s\n", len, strerror(errno));
        return false;
    }
    /* FIXME: shouldn't be necessary... we need to figure out how to
     * synchronize things */
    fflush(project->writing.strings_file);
    return true;
}

static bool bury_author(struct project *project, const char *name, size_t name_len,
        const char *email, size_t email_len, uint16_t *index)
{
    struct buried_author *author;
    uint32_t offset, dummy;

    for (size_t i = 0; i < project->writing.author_count; ++i)
    {
        char *string;

        /* FIXME: Maybe put authors in a dedicated file? */

        author = &project->writing.authors[i];
        string = dig_string_from(project, author->offset, name_len + email_len);
        if (name_len == author->name_len && email_len == author->email_len
                && !memcmp(name, string, name_len) && !memcmp(email, string + name_len, email_len))
        {
            free(string);
            *index = i;
            return true;
        }
    }

    if (!bury_string(project, name, name_len, &offset)
            || !bury_string(project, email, email_len, &dummy))
        return false;

    *index = project->writing.author_count;
    if (!array_reserve((void **)&project->writing.authors, &project->writing.authors_capacity,
            project->writing.author_count + 1, sizeof(struct buried_author)))
        return false;
    author = &project->writing.authors[project->writing.author_count++];
    author->name_len = name_len;
    author->email_len = email_len;
    author->offset = offset;
    return true;
}

static bool bury_user(struct project *project, struct json_object *user_object, uint32_t *index)
{
    size_t name_len, username_len;
    const char *name, *username;
    struct buried_user *user;
    struct json_object *tmp;
    uint32_t offset, dummy;

    if (!(tmp = get_json_value(user_object, "name")))
        return false;
    name = json_object_get_string(tmp);
    name_len = json_object_get_string_len(tmp);
    if (!(tmp = get_json_value(user_object, "username")))
        return false;
    username = json_object_get_string(tmp);
    username_len = json_object_get_string_len(tmp);

    for (size_t i = 0; i < project->writing.user_count; ++i)
    {
        char *string;

        user = &project->writing.users[i];

        if (username_len != user->username_len)
            continue;

        string = dig_string_from(project, user->offset, user->name_len + user->username_len);
        if (!memcmp(username, string + user->name_len, username_len))
        {
            if (name_len != user->name_len || memcmp(name, string, name_len))
            {
                /* Name was changed. Replace the stored name with a new one.
                 * We assume this happens infrequently enough that it's not
                 * worth trying to delete the now unused data stored where the
                 * old name was. */
                if (!bury_string(project, name, name_len, &offset)
                        || !bury_string(project, username, username_len, &dummy))
                {
                    free(string);
                    return false;
                }

                user->name_len = name_len;
                user->offset = offset;
            }

            free(string);
            *index = i;
            return true;
        }
    }

    if (!bury_string(project, name, name_len, &offset)
            || !bury_string(project, username, username_len, &dummy))
        return false;

    *index = project->writing.user_count;
    if (!array_reserve((void **)&project->writing.users, &project->writing.users_capacity,
            project->writing.user_count + 1, sizeof(struct buried_user)))
        return false;
    user = &project->writing.users[project->writing.user_count++];
    user->name_len = name_len;
    user->username_len = username_len;
    user->offset = offset;
    return true;
}

static void prefetch_patches(struct project *project, struct json_object *const *revs_arrays, size_t mr_count)
{
#ifdef HAVE_LIBGIT2
    size_t count = 0, capacity = 0;
    const char **hashes = NULL;
    git_strarray strarray;
    int ret;

    if (!project->remote)
        return;

    for (size_t i = 0; i < mr_count; ++i)
    {
        struct json_object *revs = revs_arrays[i];
        size_t rev_count = json_object_array_length(revs);

        for (size_t j = 0; j < rev_count; ++j)
        {
            struct json_object *rev = json_object_array_get_idx(revs, j);
            struct json_object *tmp;
            git_commit *commit;
            git_oid oid;

            if (!(tmp = get_json_value(rev, "head_commit_sha")))
                goto out;

            /* Check if we have it locally.
             *
             * git_remote_fetch() is pretty efficient, and giving it commits we
             * already have doesn't seem to hurt its time. However, it *does*
             * apparently have a bit of overhead (maybe 700 ms) even if we have
             * every commit.
             * By contrast, git_commit_lookup() is apparently instantaneous,
             * despite the disk access, so if we look up all the commits and we
             * already have them all, then we can skip that. */

            if ((ret = git_oid_fromstrn(&oid, json_object_get_string(tmp), json_object_get_string_len(tmp))))
            {
                err("Failed to parse head \"%s\", error %d.\n", json_object_get_string(tmp), ret);
                goto out;
            }

            if (!git_commit_lookup(&commit, project->repository, &oid))
            {
                info("Head %s (v%u) was found locally.\n", json_object_get_string(tmp), j + 1);
                git_commit_free(commit);
                continue;
            }

            info("Head %s (v%u) will be fetched from remote.\n", json_object_get_string(tmp), j + 1);

            if (!array_reserve((void **)&hashes, &capacity, count + 1, sizeof(*hashes)))
                goto out;

            hashes[count++] = json_object_get_string(tmp);
        }
    }

    if (!count)
        goto out;

    strarray.count = count;
    strarray.strings = (char **)hashes;

    info("Prefetching %zu patches...\n", count);

    if ((ret = git_remote_fetch(project->remote, &strarray, NULL, NULL)))
        err("Failed to fetch refspecs, error %d, class %u, message \"%s\".\n",
                ret, git_error_last()->klass, git_error_last()->message);

    info("Prefetch done.\n");

out:
    free(hashes);
#endif
}

/* FIXME: This needs to be a setting, and we need proper UI for it. */
#define MAX_PATCH_COUNT 50

static bool fetch_patches_local(struct project *project, uint32_t mrid,
        struct json_object *rev_object, uint16_t *ret_patch_count)
{
#ifdef HAVE_LIBGIT2
    uint16_t patch_count = 0;
    struct json_object *tmp;
    git_revwalk *revwalk;
    git_oid oid;
    int ret;

    if (!project->repository)
        return false;

    if ((ret = git_revwalk_new(&revwalk, project->repository)))
    {
        err("Failed to create revwalk, error %d.\n", ret);
        return false;
    }

    if (!(tmp = get_json_value(rev_object, "head_commit_sha")))
        goto err;
    if ((ret = git_oid_fromstrn(&oid, json_object_get_string(tmp), json_object_get_string_len(tmp))))
    {
        err("Failed to parse head \"%s\", error %d.\n", json_object_get_string(tmp), ret);
        goto err;
    }
    if ((ret = git_revwalk_push(revwalk, &oid)))
    {
        err("Failed to look up head \"%s\", error %d.\n", json_object_get_string(tmp), ret);
        goto err;
    }

    if (!(tmp = get_json_value(rev_object, "base_commit_sha")))
        goto err;
    if ((ret = git_oid_fromstrn(&oid, json_object_get_string(tmp), json_object_get_string_len(tmp))))
    {
        err("Failed to parse base \"%s\", error %d.\n", json_object_get_string(tmp), ret);
        goto err;
    }
    if ((ret = git_revwalk_hide(revwalk, &oid)))
    {
        err("Failed to look up base \"%s\", error %d.\n", json_object_get_string(tmp), ret);
        goto err;
    }

    while (!git_revwalk_next(&oid, revwalk))
    {
        const git_signature *signature;
        struct buried_patch patch;
        const char *message, *p;
        git_commit *commit;

        if ((ret = git_commit_lookup(&commit, project->repository, &oid)))
        {
            err("Failed to parse base \"%s\", error %d.\n", json_object_get_string(tmp), ret);
            goto err;
        }

        patch.flags = FLAG_UNREAD;

        /* FIXME: Use the mailmap? Maybe this should be a config option? */
        signature = git_commit_author(commit);

        if (!bury_author(project, signature->name, strlen(signature->name),
                signature->email, strlen(signature->email), &patch.author_index))
            goto err;

        message = git_commit_message(commit);
        patch.message_len = strlen(message);
        if (!bury_string(project, message, patch.message_len, &patch.message_offset))
            goto err;

        if ((p = memchr(message, '\n', patch.message_len)))
        {
            patch.subject_len = p - message;
        }
        else
        {
            err("Patch message with no newline, using whole message as subject.\n");
            patch.subject_len = patch.message_len;
        }

        if (git_commit_parentcount(commit) > 1)
        {
            git_commit_free(commit);
            err("Not storing a diff for commit %s with %u parents.\n",
                    git_oid_tostr_s(&oid), git_commit_parentcount(commit));
            patch.diff_offset = 0;
            patch.diff_len = 0;
        }
        else
        {
            git_tree *commit_tree, *parent_tree;
            git_commit *parent;
            git_buf buf = {0};
            git_diff *diff;
            size_t len;

            patch.diff_offset = ftell(project->writing.strings_file);

            if ((ret = git_commit_parent(&parent, commit, 0)))
            {
                err("Failed to get parent, error %d.\n", ret);
                git_commit_free(commit);
                goto err;
            }
            if ((ret = git_commit_tree(&commit_tree, commit)))
            {
                err("Failed to get tree, error %d.\n", ret);
                git_commit_free(parent);
                git_commit_free(commit);
                goto err;
            }
            if ((ret = git_commit_tree(&parent_tree, parent)))
            {
                err("Failed to get tree, error %d.\n", ret);
                git_tree_free(commit_tree);
                git_commit_free(parent);
                git_commit_free(commit);
                goto err;
            }

            /* FIXME: GIT_DIFF_SHOW_BINARY?
             *
             * It'd be nice if the diff algorithm was configurable, but we need
             * it to be the same algorithm that GitLab uses so that comments
             * match up.
             *
             * Should we have protection for overly large diffs?
             */
            ret = git_diff_tree_to_tree(&diff, project->repository, parent_tree, commit_tree, NULL);
            git_tree_free(commit_tree);
            git_tree_free(parent_tree);
            git_commit_free(parent);
            git_commit_free(commit);
            if (ret)
            {
                err("Failed to create diff, error %d.\n", ret);
                goto err;
            }

            if ((ret = git_diff_to_buf(&buf, diff, GIT_DIFF_FORMAT_PATCH)))
            {
                err("Failed to create diff, error %d.\n", ret);
                git_diff_free(diff);
                goto err;
            }
            git_diff_free(diff);

            patch.diff_len = buf.size;
            if ((len = fwrite(buf.ptr, 1, buf.size, project->writing.strings_file)) != buf.size)
            {
                err("Failed to write diff: %s\n", strerror(errno));
                git_buf_dispose(&buf);
                goto err;
            }
            git_buf_dispose(&buf);
        }

        /* FIXME: or buffer them all since we can fail mid-series? */
        if (fwrite(&patch, sizeof(patch), 1, project->writing.patches_file) != 1)
        {
            err("Failed to write patch: %s\n", strerror(errno));
            goto err;
        }

        ++patch_count;
        if (patch_count >= MAX_PATCH_COUNT)
        {
            /* FIXME: Terrible. */
            err("Stopping after %u commits.\n", MAX_PATCH_COUNT);
            break;
        }
    }

    info("Parsed %u commits.\n", patch_count);

    *ret_patch_count = patch_count;
    return true;

err:
    git_revwalk_free(revwalk);
#endif
    return false;
}

static bool fetch_patches(struct project *project, uint32_t mrid,
        struct json_object *rev_object, uint16_t *patch_count)
{
    struct json_object *object, *commits_object, *tmp;
    uint32_t revid;
    size_t count;
    char *url;

    if (fetch_patches_local(project, mrid, rev_object, patch_count))
        return true;

    if (!(tmp = get_json_value(rev_object, "id")))
        return false;
    revid = json_object_get_int(tmp);

    asprintf(&url, "%s/api/v4/projects/%s/merge_requests/%u/versions/%u",
            project->hostname, project->project_id, mrid, revid);
    object = do_rest_get(project, url);
    free(url);
    if (!object)
        return false;

    if (!(commits_object = get_json_value(object, "commits")))
        goto err;

    if (!validate_json_array(commits_object))
        goto err;
    count = json_object_array_length(commits_object);
    *patch_count = count;

    info("Fetching from HTTP, %zu commits.\n", count);

    if (count > MAX_PATCH_COUNT)
    {
        /* FIXME: This is terrible. Handle this better. */
        err("Merge request has %zu commits; limiting to the first %u.\n", count, MAX_PATCH_COUNT);
        count = MAX_PATCH_COUNT;
    }

    for (size_t i = 0; i < count; ++i)
    {
        struct json_object *patch_object = json_object_array_get_idx(commits_object, i);
        struct json_object *author_name, *author_email;
        const char *commit_url, *message, *p;
        struct buried_patch patch;

        patch.flags = FLAG_UNREAD;

        if (!(author_name = get_json_value(patch_object, "author_name")))
            goto err;
        if (!(author_email = get_json_value(patch_object, "author_email")))
            goto err;

        if (!bury_author(project, json_object_get_string(author_name), json_object_get_string_len(author_name),
                json_object_get_string(author_email), json_object_get_string_len(author_email), &patch.author_index))
            goto err;

        if (!(tmp = get_json_value(patch_object, "message")))
            goto err;
        message = json_object_get_string(tmp);
        patch.message_len = json_object_get_string_len(tmp);
        if (!bury_string(project, message, patch.message_len, &patch.message_offset))
            goto err;

        if ((p = memchr(message, '\n', patch.message_len)))
        {
            patch.subject_len = p - message;
        }
        else
        {
            err("Patch message with no newline, using whole message as subject.\n");
            patch.subject_len = patch.message_len;
        }

        /* FIXME: What happens if we're given a merge commit? */

        patch.diff_offset = ftell(project->writing.strings_file);

        if (!(tmp = get_json_value(patch_object, "web_url")))
            goto err;
        commit_url = json_object_get_string(tmp);

        /* We might have already been given a diff.
         * Unfortunately, it's a squashed version of all the commits,
         * and it's also in a partially parsed format.
         * FIXME: We could still potentially use it if there's only one commit,
         * though, and unparse it to get the original git diff... */

        asprintf(&url, "%s.diff", commit_url);
        if (!fetch_diff(project->writing.curl, url,
                project->writing.strings_file, &patch.diff_len))
        {
            free(url);
            goto err;
        }
        free(url);

        /* FIXME: or buffer them all since we can fail mid-series? */
        if (fwrite(&patch, sizeof(patch), 1, project->writing.patches_file) != 1)
        {
            err("Failed to write patch: %s\n", strerror(errno));
            goto err;
        }
    }

    return true;

err:
    json_object_put(object);
    return false;
}

static bool fetch_revs(struct project *project, struct json_object *object,
        const struct buried_mr *existing_mr, uint32_t mrid, uint16_t *rev_count)
{
    struct json_object *tmp;
    size_t count;
    size_t i = 0;

    count = json_object_array_length(object);

    /* We are assuming that GitLab will never delete a merge request
     * version under our feet. This is probably a slightly dangerous
     * assumption, since I have seen it delete entire MRs before... */
    if (existing_mr)
    {
        if (fwrite(&project->revs[existing_mr->revs_offset], sizeof(struct buried_rev),
                existing_mr->rev_count, project->writing.revs_file) != existing_mr->rev_count)
        {
            err("Failed to write revs: %s\n", strerror(errno));
            goto err;
        }

        i = existing_mr->rev_count;
    }

    info("Merge request has %u revisions, of which we already have %u.\n", count, i);

    for (; i < count; ++i)
    {
        struct json_object *rev_object = json_object_array_get_idx(object, i);
        struct buried_rev rev;

        rev.flags = FLAG_UNREAD;

        if (!(tmp = get_json_value(rev_object, "created_at")))
            goto err;
        if (!parse_time(json_object_get_string(tmp), json_object_get_string_len(tmp), &rev.time))
            goto err;

        rev.patches_offset = ftell(project->writing.patches_file) / sizeof(struct buried_patch);

        info("Looking up mr %u, revision %u...\n", mrid, i);

        if (!fetch_patches(project, mrid, rev_object, &rev.patch_count))
            goto err;

        /* FIXME: or buffer them all since we can fail mid-series? */
        if (fwrite(&rev, sizeof(rev), 1, project->writing.revs_file) != 1)
        {
            err("Failed to write rev: %s\n", strerror(errno));
            goto err;
        }
    }

    *rev_count = count;
    return true;

err:
    return false;
}

static bool parse_note_id(struct json_object *object, uint32_t *id)
{
    const char *s = json_object_get_string(object);

    if (sscanf(s, "gid://gitlab/Note/%u", id) != 1
            && sscanf(s, "gid://gitlab/DiscussionNote/%u", id) != 1
            && sscanf(s, "gid://gitlab/DiffNote/%u", id) != 1)
    {
        err("Failed to parse note ID from \"%s\".\n", s);
        return false;
    }

    return true;
}

static const struct buried_reply *find_reply_by_id(
        const struct buried_reply *replies, size_t count, uint32_t id)
{
    for (size_t i = 0; i < count; ++i)
    {
        if (replies[i].id == id)
            return &replies[i];
    }

    return NULL;
}

static bool fetch_replies(struct project *project, const struct buried_comment *existing_comment,
        struct json_object *replies_array, uint16_t *reply_count)
{
    size_t count;

    count = json_object_array_length(replies_array);
    for (size_t j = 0; j < count; ++j)
    {
        struct json_object *reply_object = json_object_array_get_idx(replies_array, j);
        const struct buried_reply *existing_reply;
        struct buried_reply reply = {0};
        struct json_object *tmp;
        const char *body;
        uint32_t id;

        if (!(tmp = get_json_value(reply_object, "id")))
            return false;
        if (!parse_note_id(tmp, &id))
            return false;

        if (existing_comment && (existing_reply = find_reply_by_id(
                &project->replies[existing_comment->u.comment.replies_offset],
                existing_comment->u.comment.reply_count, id)))
        {
            reply = *existing_reply;
        }
        else
        {
            reply.flags = FLAG_UNREAD;
            reply.id = id;

            info("Found new reply %u.\n", id);

            if (!(tmp = get_json_value(reply_object, "author")))
                return false;
            if (!bury_user(project, tmp, &reply.user_index))
                return false;

            if (!(tmp = get_json_value(reply_object, "createdAt")))
                return false;
            if (!parse_time(json_object_get_string(tmp), json_object_get_string_len(tmp), &reply.timestamp))
                return false;

            if (!(tmp = get_json_value(reply_object, "body")))
                return false;
            body = json_object_get_string(tmp);
            reply.content_len = json_object_get_string_len(tmp);
            if (!bury_string(project, body, reply.content_len, &reply.content_offset))
                return false;
        }

        if (fwrite(&reply, sizeof(reply), 1, project->writing.replies_file) != 1)
        {
            err("Failed to write reply: %s\n", strerror(errno));
            return false;
        }
    }

    *reply_count = count;
    return true;
}

static const struct buried_comment *find_comment_by_id(
        const struct buried_comment *comments, size_t count, uint32_t id)
{
    for (size_t i = 0; i < count; ++i)
    {
        if (comments[i].id == id)
            return &comments[i];
    }

    return NULL;
}

/* Some comments are attached to specific patches, but we can't really link
 * them together in our stored data until we've parsed both.
 *
 * Hence this function parses all the comments for a MR into a flat array, and
 * we'll assign them to specific comments and bury them later. We do bury the
 * replies and strings now, though. */
static bool chew_comments(struct project *project, const struct buried_mr *existing_mr,
        struct json_object *discussions_object, struct chewed_comment **ret_comments, uint32_t *ret_comment_count)
{
    struct json_object *discussions_array;
    struct chewed_comment *comments;
    size_t count, comment_count = 0;

    if (!(discussions_array = get_json_value(discussions_object, "nodes")))
        return false;
    if (!validate_json_array(discussions_array))
        return false;

    count = json_object_array_length(discussions_array);
    if (!(comments = calloc(count, sizeof(*comments))))
        return false;

    for (size_t i = 0; i < count; ++i)
    {
        struct json_object *comment_object = json_object_array_get_idx(discussions_array, i);
        struct json_object *replies_object, *replies_array, *note, *metadata, *tmp;
        struct chewed_comment *comment = &comments[comment_count];
        const struct buried_comment *existing_comment;
        uint32_t id;

        if (!(replies_object = get_json_value(comment_object, "notes")))
            goto err;
        if (!(replies_array = get_json_value(replies_object, "nodes")))
            goto err;
        if (!validate_json_array(replies_array))
            return false;

        if (!json_object_array_length(replies_array))
        {
            err("Unexpected empty notes array.\n");
            goto err;
        }

        note = json_object_array_get_idx(replies_array, 0);

        if (!(tmp = get_json_value(note, "id")))
            goto err;
        if (!parse_note_id(tmp, &id))
            goto err;

        /* FIXME: Comments can be deleted. I don't think we want to delete them
         * from our store in that case. */

        if (existing_mr && (existing_comment = find_comment_by_id(
                &project->comments[existing_mr->comments_offset], existing_mr->comment_count, id)))
        {
            info("Updating comment %u...\n", id);

            comment->buried = *existing_comment;
            comment->new = false;

            if (existing_comment->type == COMMENT_TYPE_COMMENT)
            {
                comment->buried.u.comment.replies_offset =
                        ftell(project->writing.replies_file) / sizeof(struct buried_reply);
                if (!fetch_replies(project, existing_comment, replies_array, &comment->buried.u.comment.reply_count))
                    goto err;
            }

            ++comment_count;
            continue;
        }

        comment->buried.id = id;
        comment->new = true;

        if ((metadata = get_json_value(note, "systemNoteMetadata")))
        {
            const char *action;

            if (json_object_array_length(replies_array) > 1)
            {
                err("Unexpected notes array length %zu for a system note.\n",
                        json_object_array_length(replies_array));
                goto err;
            }

            if (!(tmp = get_json_value(metadata, "action")))
                goto err;
            action = json_object_get_string(tmp);

            if (!strcmp(action, "commit"))
            {
                /* Ignore. (No, unfortunately this does not substitute for the
                 * revisions API. */
                continue;
            }
            else if (!strcmp(action, "approved"))
            {
                comment->buried.type = COMMENT_TYPE_APPROVE;
            }
            else if (!strcmp(action, "unapproved"))
            {
                comment->buried.type = COMMENT_TYPE_UNAPPROVE;
            }
            else if (!strcmp(action, "title") || !strcmp(action, "reviewer")
                    || !strcmp(action, "assignee") || !strcmp(action, "cross_reference"))
            {
                /* FIXME: It'd probably be nice to replicate these.
                 *
                 * Unfortunately the data isn't available in a structured
                 * format, which means we'll just have to write them verbatim. */
                continue;
            }
            else
            {
                err("Unknown system action \"%s\".\n", action);
                continue;
            }

            info("Adding new system comment %u...\n", id);

            if (!(tmp = get_json_value(note, "author")))
                return false;
            if (!bury_user(project, tmp, &comment->buried.u.activity.user_index))
                return false;

            if (!(tmp = get_json_value(note, "createdAt")))
                return false;
            if (!parse_time(json_object_get_string(tmp), json_object_get_string_len(tmp),
                    &comment->buried.u.activity.timestamp))
                return false;
        }
        else
        {
            struct json_object *position;

            info("Adding new comment %u...\n", id);

            comment->buried.type = COMMENT_TYPE_COMMENT;

            comment->buried.u.comment.replies_offset =
                    ftell(project->writing.replies_file) / sizeof(struct buried_reply);

            if (!fetch_replies(project, NULL, replies_array, &comment->buried.u.comment.reply_count))
                goto err;

            if ((position = get_json_value(note, "position")))
            {
                comment->is_patch_comment = true;

                if (!(tmp = get_json_value(position, "diffRefs")))
                    goto err;
                if (!(tmp = get_json_value(tmp, "headSha")))
                    goto err;
                comment->sha = json_object_get_string(tmp);

                if ((tmp = get_json_value(position, "oldLine")))
                    comment->old_line = json_object_get_int(tmp);
                if ((tmp = get_json_value(position, "newLine")))
                    comment->new_line = json_object_get_int(tmp);

                if (!(tmp = get_json_value(position, "oldPath")))
                    goto err;
                comment->old_path = json_object_get_string(tmp);
                if (!(tmp = get_json_value(position, "newPath")))
                    goto err;
                comment->old_path = json_object_get_string(tmp);
            }
        }

        ++comment_count;
    }

    /* For some unfathomable reason, "system" events include approvals,
     * but not open/close/merge. There's a different API for that, and,
     * hilariously, it seems to be yet another thing that was never ported
     * to GraphQL. */

    *ret_comments = comments;
    *ret_comment_count = comment_count;
    return true;

err:
    free(comments);
    return false;
}

static struct buried_mr *find_mr_by_id(struct buried_mr *mrs, size_t count, uint32_t mrid)
{
    for (size_t i = 0; i < count; ++i)
    {
        if (mrs[i].mrid == mrid)
            return &mrs[i];
    }

    return NULL;
}

#define TIMESTAMP_LEN 20

static bool fetch_mrs(struct project *project)
{
    size_t req_len, mr_count, page_after_len, then_string_len, now_string_len;
    struct json_object *object, *tmp, *mrs_object, *nodes_object;
    char *req, *mr_args, *page_after, *then_string;
    struct json_object **revs_arrays = NULL;
    char now_string[TIMESTAMP_LEN + 1];
    size_t fetched_count = 0;
    struct buried_mr *mrs;
    bool write_timestamp;

    /* Gitlab implements graphql queries by shoving them into a trivial JSON
     * container first. I don't know why.
     *
     * For now just do the encoding by hand (even though we have a library
     * handy), since it is pretty trivial.
     * Just a couple of quotes we need to escape. */
    static const char req_template[] =
        "{\"query\": \"query {"
        "    project(fullPath: \\\"%s\\\") {"
        "        mergeRequests(%s) {"
        "            nodes {"
        "                iid"
        "                title"
        "                author {name username}"
        "                description"
        /* There doesn't seem to be a way to filter comments by time. */
        "                discussions {"
        "                    nodes {"
        "                        notes {"
        "                            nodes {"
        "                                author {name username}"
        "                                body"
        "                                createdAt"
        "                                id"
        "                                position {"
        "                                    diffRefs {headSha}"
        "                                    oldLine"
        "                                    oldPath"
        "                                    newLine"
        "                                    newPath"
        "                                }"
        "                                systemNoteMetadata {action}"
        "                            }"
        "                            pageInfo {endCursor hasNextPage}"
        "                        }"
        "                    }"
        "                    pageInfo {endCursor hasNextPage}"
        "                }"
        "            }"
        "            pageInfo {endCursor hasNextPage}"
        "        }"
        "    }"
        "}\"}";

    if (!(then_string = dig_db(project, "timestamp", 1, &then_string_len)))
    {
        then_string = strdup("2022-05-08T02:52:51Z");
        then_string_len = strlen(then_string);
    }

    /* Copy the previous list, and update it as necessary. We're not retrieving
     * all the MRs with the following request. */

    mr_count = project->mr_count;
    mrs = malloc(mr_count * sizeof(struct buried_mr));
    memcpy(mrs, project->mrs, mr_count * sizeof(struct buried_mr));

    /* We'd like to limit the number of round trips we have to do to the server.
     *
     * On the other hand, the time the server takes to reply is directly
     * proportional to the amount of data it has to send (and apparently this
     * isn't just because of TCP transmission overhead, but because Gitlab
     * and/or its underlying database software is very poorly optimized), and
     * we'd like to be at least a little responsive.
     *
     * That is, if we're fetching a large amount of data (e.g. because we're
     * doing an initial download), it's nicer to the user to update the UI with
     * new merge requests every so often, instead of waiting a long time and
     * then updating them all at once. It also means we won't lose everything
     * if we're killed or closed mid-fetch.
     *
     * Moreover... remember the part about Gitlab being badly optimized?
     * If you make a query that takes too long on the server side (seems to be
     * around 30 seconds), the query will just time out. Gitlab will report to
     * you that the query has timed out, but of course it doesn't do so in a
     * parseable way; it returns a (presumably) localized string. So we have no
     * reliable way of knowing if we timed out.
     *
     * Fetching 5 merge requests at a time takes a pretty inconsistent amount
     * of time, probably depending on how many comments there are. But running
     * it on the vkd3d repository a few times in succession, so grabbing new
     * merge requests every time, I see a median of about 2.6 seconds, maximum
     * of 8.6, which seems pretty decent for UI purposes, and hopefully far
     * enough below the server-side timeout of 30 seconds. */

    if ((page_after = dig_db(project, "mr_page", 1, &page_after_len)))
    {
        asprintf(&mr_args, "sort: UPDATED_DESC, first: 5, after: \\\"%.*s\\\"",
                (int)page_after_len, page_after);
        write_timestamp = false;
    }
    else
    {
        struct tm now_tm;
        time_t now;

        asprintf(&mr_args, "sort: UPDATED_DESC, first: 5, updatedAfter: \\\"%.*s\\\"",
                (int)then_string_len, then_string);

        /* Write the new timestamp only if we are starting a new fetch.
         * If we are continuing a fetch operation, we want the timestamp after
         * we're done to point to the timestamp when we started the *first*
         * batch of this fetch. */

        time(&now);
        gmtime_r(&now, &now_tm);
        if (!(now_string_len = strftime(now_string, sizeof(now_string), "%Y-%m-%dT%H:%M:%SZ", &now_tm)))
            die("Current time unexpectedly overflows.\n");
        write_timestamp = true;
    }

    req_len = asprintf(&req, req_template, project->project_path, mr_args);
    free(mr_args);
    object = do_graphql_query(project, req, req_len);
    free(req);
    if (!object)
    {
        free(mrs);
        return false;
    }

    if (!(tmp = get_json_value(object, "data")))
        goto err;
    if (!(tmp = get_json_value(tmp, "project")))
        goto err;
    if (!(mrs_object = get_json_value(tmp, "mergeRequests")))
        goto err;
    if (!(nodes_object = get_json_value(mrs_object, "nodes")))
        goto err;
    if (!validate_json_array(nodes_object))
        goto err;

    fetched_count = json_object_array_length(nodes_object);

    /* We may want to consider fetching these arrays simultaneously.
     * Of course, I don't know at what point we're making too many requests... */

    revs_arrays = calloc(fetched_count, sizeof(*revs_arrays));
    for (size_t i = 0; i < fetched_count; ++i)
    {
        struct json_object *mr_object = json_object_array_get_idx(nodes_object, i);
        uint32_t mrid;
        char *url;

        if (!(tmp = get_json_value(mr_object, "iid")))
            goto err;
        mrid = json_object_get_int(tmp);

        /* We might have some of these already, but we can't ask for a limited
         * range, and making a series of individual requests is probably worse in
         * general than making a single request, especially considering that
         * usually there will be less than 10 or so versions.
         *
         * Of course we might need to ask for individual versions anyway, to get
         * the commits. But in practice we should be able to fetch them locally,
         * especially in the course of regular operation fetching new MRs. */

        asprintf(&url, "%s/api/v4/projects/%s/merge_requests/%u/versions",
                project->hostname, project->project_id, mrid);
        if (!(revs_arrays[i] = do_rest_get(project, url)))
        {
            free(url);
            goto err;
        }
        free(url);
        if (!validate_json_array(revs_arrays[i]))
            goto err;
    }

    if (fetched_count)
        prefetch_patches(project, revs_arrays, fetched_count);

    for (size_t i = 0; i < fetched_count; ++i)
    {
        struct json_object *mr_object = json_object_array_get_idx(nodes_object, i);
        struct buried_mr *mr, *existing_mr = NULL;
        struct chewed_comment *comments;
        uint32_t comment_count;
        uint32_t mrid;

        if (!(tmp = get_json_value(mr_object, "iid")))
            goto err;
        mrid = json_object_get_int(tmp);

        if ((existing_mr = find_mr_by_id(mrs, mr_count, mrid)))
        {
            mr = existing_mr;

            info("Updating merge request %u.\n", mrid);

            mr->flags |= FLAG_UPDATED;

            /* We may consider updating merge request descriptions, but I quite
             * honestly consider this an anti-feature. I would, as a user,
             * prefer to see the original title, even if we do record title
             * change events. Of course, we could make this configurable... */
        }
        else
        {
            if (!(mrs = realloc(mrs, (mr_count + 1) * sizeof(*mrs))))
                goto err;
            mr = &mrs[mr_count++];

            info("Fetching new merge request %u.\n", mrid);

            mr->mrid = mrid;

            if (!(tmp = get_json_value(mr_object, "author")))
                goto err;
            if (!bury_user(project, tmp, &mr->user_index))
                goto err;

            mr->flags = FLAG_UPDATED | FLAG_UNREAD;

            if (!(tmp = get_json_value(mr_object, "title")))
                goto err;
            mr->title_len = json_object_get_string_len(tmp);
            if (!bury_string(project, json_object_get_string(tmp), mr->title_len, &mr->title_offset))
                goto err;

            if (!(tmp = get_json_value(mr_object, "description")))
                goto err;
            mr->desc_len = json_object_get_string_len(tmp);
            if (mr->desc_len)
            {
                if (!bury_string(project, json_object_get_string(tmp), mr->desc_len, &mr->desc_offset))
                    goto err;
            }
        }

        if (!(tmp = get_json_value(mr_object, "discussions")))
            goto err;
        if (!chew_comments(project, existing_mr, tmp, &comments, &comment_count))
            goto err;

        /* FIXME */
        mr->comment_count = comment_count;
        mr->comments_offset = ftell(project->writing.comments_file) / sizeof(struct buried_comment);
        for (size_t j = 0; j < comment_count; ++j)
        {
            if (fwrite(&comments[j].buried, sizeof(struct buried_comment), 1, project->writing.comments_file) != 1)
            {
                err("Failed to write comment: %s\n", strerror(errno));
                goto err;
            }
        }

        mr->revs_offset = ftell(project->writing.revs_file) / sizeof(struct buried_rev);
        if (!fetch_revs(project, revs_arrays[i], existing_mr, mr->mrid, &mr->rev_count))
            goto err;
    }

    if (!(tmp = get_json_value(mrs_object, "pageInfo")))
        goto err;
    if (!(tmp = get_json_value(tmp, "hasNextPage")))
        goto err;
    if (json_object_get_boolean(tmp))
    {
        FILE *page_file;

        if (!(tmp = get_json_value(mrs_object, "pageInfo")))
            goto err;
        if (!(tmp = get_json_value(tmp, "endCursor")))
            goto err;

        if (!(page_file = fopen_data_path(project, "mr_page", "wb")))
        {
            err("Failed to open page file: %s\n", strerror(errno));
            goto err;
        }
        if (fwrite(json_object_get_string(tmp), 1, json_object_get_string_len(tmp),
                page_file) != json_object_get_string_len(tmp))
        {
            err("Failed to write \"%s\" to page file: %s\n",
                    json_object_get_string(tmp), strerror(errno));
            fclose(page_file);
            goto err;
        }
        fclose(page_file);
    }
    else
    {
        char *page_path = get_data_path(project, "mr_page");

        if (unlink(page_path) < 0 && errno != ENOENT)
            err("Failed to remove %s: %s\n", page_path, strerror(errno));
        free(page_path);
    }

    if (write_timestamp)
        bury_db(project, "timestamp", now_string, 1, now_string_len);

    for (size_t i = 0; i < mr_count; ++i)
    {
        struct buried_mr *mr = &mrs[i];

        if (mr->flags & FLAG_UPDATED)
        {
            /* All its data is already written. */
            mr->flags &= ~FLAG_UPDATED;
        }
        else
        {
            uint32_t prev_revs_offset = mr->revs_offset;

            /* Revs have patches, but patches are immutable and already
             * written, so we can just blit the revs verbatim. */

            mr->revs_offset = ftell(project->writing.revs_file) / sizeof(struct buried_rev);
            if (fwrite(&project->revs[prev_revs_offset], sizeof(struct buried_rev),
                    mr->rev_count, project->writing.revs_file) != mr->rev_count)
            {
                err("Failed to write revs: %s\n", strerror(errno));
                goto err;
            }

            /* Comments have replies, though. */
            for (size_t j = 0; j < mr->comment_count; ++j)
            {
                struct buried_comment *comment = &project->comments[mr->comments_offset + j];
                uint32_t prev_replies_offset;

                if (comment->type == COMMENT_TYPE_COMMENT)
                {
                    /* Replies only point to immutable data; blit them verbatim. */
                    prev_replies_offset = comment->u.comment.replies_offset;
                    comment->u.comment.replies_offset =
                            ftell(project->writing.replies_file) / sizeof(struct buried_reply);
                    if (fwrite(&project->replies[prev_replies_offset], sizeof(struct buried_reply),
                            comment->u.comment.reply_count, project->writing.replies_file) != comment->u.comment.reply_count)
                    {
                        err("Failed to write replies: %s\n", strerror(errno));
                        goto err;
                    }
                }

                if (fwrite(comment, sizeof(*comment), 1, project->writing.comments_file) != 1)
                {
                    err("Failed to write comment: %s\n", strerror(errno));
                    goto err;
                }
            }
        }
    }

    if (fwrite(mrs, sizeof(*mrs), mr_count, project->writing.mrs_file) != mr_count)
    {
        err("Failed to write mrs: %s\n", strerror(errno));
        goto err;
    }

    if (revs_arrays)
    {
        for (size_t i = 0; i < fetched_count; ++i)
            json_object_put(revs_arrays[i]);
    }
    free(mrs);
    json_object_put(object);
    return true;

err:
    if (revs_arrays)
    {
        for (size_t i = 0; i < fetched_count; ++i)
        {
            if (revs_arrays[i])
                json_object_put(revs_arrays[i]);
        }
    }
    free(mrs);
    json_object_put(object);
    return false;
}

static void fetch_project(struct project *project)
{
    /* FIXME: This doesn't belong here. */
    curl_global_init(CURL_GLOBAL_ALL);

    memset(&project->writing, 0, sizeof(project->writing));

    project->writing.curl = curl_easy_init();

    project->writing.author_count = project->author_count;
    array_reserve((void **)&project->writing.authors, &project->writing.authors_capacity,
            project->author_count, sizeof(struct buried_author));
    memcpy(project->writing.authors, project->authors, project->author_count * sizeof(struct buried_author));

    project->writing.user_count = project->user_count;
    array_reserve((void **)&project->writing.users, &project->writing.users_capacity,
            project->user_count, sizeof(struct buried_user));
    memcpy(project->writing.users, project->users, project->user_count * sizeof(struct buried_user));

    project->writing.strings_file = fopen_data_path(project, "strings", "ab");
    /* FIXME ZF: mrs and revs need the dance to protect the data.
     * Move the old file to .old, create the new at .new, move it into place
     * once done. */
    project->writing.mrs_file = fopen_data_path(project, "mrs0", "wb");
    project->writing.revs_file = fopen_data_path(project, "revs0", "wb");
    project->writing.comments_file = fopen_data_path(project, "comments0", "wb");
    project->writing.replies_file = fopen_data_path(project, "replies0", "wb");
    project->writing.patches_file = fopen_data_path(project, "patches0", "ab");

    if (fetch_mrs(project))
    {
        bury_db(project, "authors0", project->writing.authors,
                sizeof(struct buried_author), project->writing.author_count);
        bury_db(project, "users0", project->writing.users,
                sizeof(struct buried_user), project->writing.user_count);

        fclose(project->writing.mrs_file);
        fclose(project->writing.patches_file);
        fclose(project->writing.revs_file);
        fclose(project->writing.strings_file);
        fclose(project->writing.comments_file);
        fclose(project->writing.replies_file);
    }
    else
    {
        err("Failed to fetch merge requests.\n");
        /* FIXME: cleanup. */
    }

    curl_easy_cleanup(project->writing.curl);
}

void fetch(void)
{
    for (size_t i = 0; i < project_count; ++i)
        fetch_project(&projects[i]);
}

void labrador_init(void)
{
    init_config();

    for (size_t i = 0; i < project_count; ++i)
        dig_mrs(&projects[i]);
}

/* Index handling for active_project->patches list.
 *
 * The indices API is terrible (though also, unfortunately, the best performance
 * that doesn't require reimplementing the entire control.)
 * The QModelIndex is supposed to be an ephemeral struct, but Qt (or at least
 * QTreeView) requires the *internal pointer* to be stable *and* unique.
 * This fact isn't documented anywhere.
 *
 * Currently the internal pointer points within the buried mrs / revs / patches
 * array (respectively). We do pointer comparison to figure out which one it
 * originally was (which is against spec but hopefully works anyway). */

static struct buried_mr *get_mr_from_index(const struct model_index *index)
{
    struct buried_mr *mr = index->internal;

    if (mr >= active_project->mrs && mr < active_project->mrs + active_project->mr_count)
        return mr;
    return NULL;
}

static struct buried_rev *get_rev_from_index(const struct model_index *index)
{
    struct buried_rev *rev = index->internal;

    if (rev >= active_project->revs && rev < active_project->revs + active_project->rev_count)
        return rev;
    return NULL;
}

struct buried_patch *get_patch_from_index(const struct model_index *index)
{
    struct buried_patch *patch = index->internal;

    if (patch >= active_project->patches && patch < active_project->patches + active_project->patch_count)
        return patch;
    return NULL;
}

static struct buried_comment *get_comment_from_index(const struct model_index *index)
{
    struct buried_comment *comment = index->internal;

    if (comment >= active_project->comments && comment < active_project->comments + active_project->comment_count)
        return comment;
    return NULL;
}

static struct buried_reply *get_reply_from_index(const struct model_index *index)
{
    struct buried_reply *reply = index->internal;

    if (reply >= active_project->replies && reply < active_project->replies + active_project->reply_count)
        return reply;
    return NULL;
}

int patch_list_get_row_count(const struct model_index *parent)
{
    const struct buried_comment *comment;
    const struct buried_rev *rev;
    const struct buried_mr *mr;

    if (!parent)
    {
        return active_project->mr_count;
    }
    else if ((mr = get_mr_from_index(parent)))
    {
        return mr->rev_count + mr->comment_count;
    }
    else if ((rev = get_rev_from_index(parent)))
    {
        return rev->patch_count;
    }
    else if ((comment = get_comment_from_index(parent)))
    {
        if (comment->type == COMMENT_TYPE_COMMENT)
            return comment->u.comment.reply_count - 1;
        else
            return 0;
    }
    else
    {
        /* patch */
        return 0;
    }
}

void patch_list_get_index(struct model_index *index, int row, int column, const struct model_index *parent)
{
    const struct buried_comment *comment;
    const struct buried_rev *rev;
    const struct buried_mr *mr;

    index->row = row;
    index->column = column;

    /* The buried lists are ordered newest first; this is what gitlab gives us.
     * No idea if this is reliable. */

    if (!parent)
    {
        if (newest_top)
            index->internal = &active_project->mrs[row];
        else
            index->internal = &active_project->mrs[active_project->mr_count - 1 - row];
    }
    else if ((mr = get_mr_from_index(parent)))
    {
        /* FIXME: Properly sort by time.
         *
         * FIXME 2: Would it make sense to merge revisions and comments into
         * the same db, as "activity"? */

        if (row < mr->rev_count)
        {
            if (newest_top)
                index->internal = &active_project->revs[mr->revs_offset + row];
            else
                index->internal = &active_project->revs[mr->revs_offset + (mr->rev_count - 1 - row)];
        }
        else
        {
            row -= mr->rev_count;

            /* Comments and activity, on the other hand, are oldest first. */
            if (newest_top)
                index->internal = &active_project->comments[mr->comments_offset + (mr->comment_count - 1 - row)];
            else
                index->internal = &active_project->comments[mr->comments_offset + row];
        }
    }
    else if ((rev = get_rev_from_index(parent)))
    {
        if (newest_top)
            index->internal = &active_project->patches[rev->patches_offset + row];
        else
            index->internal = &active_project->patches[rev->patches_offset + (rev->patch_count - 1 - row)];
    }
    else if ((comment = get_comment_from_index(parent)))
    {
        /* Replies are oldest first. */
        assert(comment->type == COMMENT_TYPE_COMMENT);
        if (newest_top)
            index->internal = &active_project->replies[comment->u.comment.replies_offset + 1 + (comment->u.comment.reply_count - 2 - row)];
        else
            index->internal = &active_project->replies[comment->u.comment.replies_offset + 1 + row];
    }
    else
    {
        assert(0);
    }
}

/* FIXME: These are inefficient, and I suspect that it matters.
 * We should probably construct a reverse lookup array. */

static uint32_t mr_index_from_rev(const struct buried_rev *rev)
{
    size_t rev_offset = rev - active_project->revs;

    for (size_t i = 0; i < active_project->mr_count; ++i)
    {
        const struct buried_mr *mr = &active_project->mrs[i];

        if (rev_offset >= mr->revs_offset && rev_offset < mr->revs_offset + mr->rev_count)
            return i;
    }

    assert(0);
}

static uint32_t rev_index_from_patch(const struct buried_patch *patch)
{
    size_t patch_offset = patch - active_project->patches;

    for (size_t i = 0; i < active_project->rev_count; ++i)
    {
        const struct buried_rev *rev = &active_project->revs[i];

        if (patch_offset >= rev->patches_offset && patch_offset < rev->patches_offset + rev->patch_count)
            return i;
    }

    assert(0);
}

static uint32_t mr_index_from_comment(const struct buried_comment *comment)
{
    size_t comment_offset = comment - active_project->comments;

    for (size_t i = 0; i < active_project->mr_count; ++i)
    {
        const struct buried_mr *mr = &active_project->mrs[i];

        if (comment_offset >= mr->comments_offset && comment_offset < mr->comments_offset + mr->comment_count)
            return i;
    }

    assert(0);
}

static uint32_t comment_index_from_reply(const struct buried_reply *reply)
{
    size_t reply_offset = reply - active_project->replies;

    for (size_t i = 0; i < active_project->comment_count; ++i)
    {
        const struct buried_comment *comment = &active_project->comments[i];

        if (comment->type == COMMENT_TYPE_COMMENT
                && reply_offset >= comment->u.comment.replies_offset
                && reply_offset < comment->u.comment.replies_offset + comment->u.comment.reply_count)
            return i;
    }

    assert(0);
}

bool patch_list_get_parent(struct model_index *parent, const struct model_index *index)
{
    const struct buried_comment *comment;
    const struct buried_patch *patch;
    const struct buried_reply *reply;
    const struct buried_rev *rev;
    const struct buried_mr *mr;

    if ((mr = get_mr_from_index(index)))
    {
        return false;
    }
    else if ((rev = get_rev_from_index(index)))
    {
        uint32_t index = mr_index_from_rev(rev);

        parent->row = index;
        parent->column = 0;
        parent->internal = &active_project->mrs[index];
        return true;
    }
    else if ((patch = get_patch_from_index(index)))
    {
        uint32_t index = rev_index_from_patch(patch);

        parent->row = index;
        parent->column = 0;
        parent->internal = &active_project->revs[index];
        return true;
    }
    else if ((comment = get_comment_from_index(index)))
    {
        uint32_t index = mr_index_from_comment(comment);

        /* FIXME: Patch comments. */

        parent->row = index;
        parent->column = 0;
        parent->internal = &active_project->mrs[index];
        return true;
    }
    else if ((reply = get_reply_from_index(index)))
    {
        uint32_t index = comment_index_from_reply(reply);

        parent->row = index;
        parent->column = 0;
        parent->internal = &active_project->comments[index];
        return true;
    }
    else
    {
        assert(0);
    }
}

char *patch_list_get_text(const struct model_index *index, size_t *size)
{
    const struct buried_comment *comment;
    const struct buried_patch *patch;
    const struct buried_reply *reply;
    const struct buried_rev *rev;
    const struct buried_mr *mr;
    char *ret;

    if ((mr = get_mr_from_index(index)))
    {
        if (index->column == PATCH_LIST_COLUMN_NAME)
        {
            char *title;

            title = dig_string(mr->title_offset, mr->title_len);
            *size = asprintf(&ret, "[%u] %.*s", mr->mrid, mr->title_len, title);
            return ret;
        }
        else if (index->column == PATCH_LIST_COLUMN_AUTHOR)
        {
            const struct buried_user *user = &active_project->users[mr->user_index];

            *size = user->name_len;
            return dig_string(user->offset, user->name_len);
        }
    }
    else if ((rev = get_rev_from_index(index)))
    {
        if (index->column == PATCH_LIST_COLUMN_NAME)
        {
            if (newest_top)
            {
                const struct buried_mr *mr = &active_project->mrs[mr_index_from_rev(rev)];

                *size = asprintf(&ret, "[v%u]", (mr->rev_count - 1 - index->row) + 1);
            }
            else
            {
                *size = asprintf(&ret, "[v%u]", index->row + 1);
            }
            return ret;
        }
        else if (index->column == PATCH_LIST_COLUMN_DATE)
        {
            return format_date(rev->time, size);
        }
    }
    else if ((patch = get_patch_from_index(index)))
    {
        if (index->column == PATCH_LIST_COLUMN_NAME)
        {
            const struct buried_rev *rev = &active_project->revs[rev_index_from_patch(patch)];
            uint32_t patch_number;
            char *title;

            title = dig_string(patch->message_offset, patch->subject_len);
            if (newest_top)
                patch_number = (rev->patch_count - 1 - index->row) + 1;
            else
                patch_number = index->row + 1;
            *size = asprintf(&ret, "[%u/%u] %.*s", patch_number, rev->patch_count, patch->subject_len, title);
            return ret;
        }
        else if (index->column == PATCH_LIST_COLUMN_AUTHOR)
        {
            const struct buried_author *author = &active_project->authors[patch->author_index];

            *size = author->name_len;
            return dig_string(author->offset, author->name_len);
        }
    }
    else if ((comment = get_comment_from_index(index)))
    {
        if (index->column == PATCH_LIST_COLUMN_NAME)
        {
            switch (comment->type)
            {
                /* FIXME: Translate these. */
                case COMMENT_TYPE_COMMENT:
                    *size = asprintf(&ret, "[comment]");
                    return ret;

                case COMMENT_TYPE_CLOSE:
                    *size = asprintf(&ret, "[closed]");
                    return ret;

                case COMMENT_TYPE_REOPEN:
                    *size = asprintf(&ret, "[reopened]");
                    return ret;

                case COMMENT_TYPE_APPROVE:
                    *size = asprintf(&ret, "[approved]");
                    return ret;

                case COMMENT_TYPE_UNAPPROVE:
                    *size = asprintf(&ret, "[unapproved]");
                    return ret;
            }
        }
        else if (index->column == PATCH_LIST_COLUMN_AUTHOR)
        {
            if (comment->type == COMMENT_TYPE_COMMENT)
            {
                const struct buried_reply *reply = &active_project->replies[comment->u.comment.replies_offset];
                const struct buried_user *user = &active_project->users[reply->user_index];

                *size = user->name_len;
                return dig_string(user->offset, user->name_len);
            }
            else
            {
                const struct buried_user *user = &active_project->users[comment->u.activity.user_index];

                *size = user->name_len;
                return dig_string(user->offset, user->name_len);
            }
        }
        else if (index->column == PATCH_LIST_COLUMN_DATE)
        {
            if (comment->type == COMMENT_TYPE_COMMENT)
            {
                const struct buried_reply *reply = &active_project->replies[comment->u.comment.replies_offset];

                return format_date(reply->timestamp, size);
            }
            else
            {
                return format_date(comment->u.activity.timestamp, size);
            }
        }
    }
    else if ((reply = get_reply_from_index(index)))
    {
        if (index->column == PATCH_LIST_COLUMN_AUTHOR)
        {
            const struct buried_user *user = &active_project->users[reply->user_index];

            *size = user->name_len;
            return dig_string(user->offset, user->name_len);
        }
        else if (index->column == PATCH_LIST_COLUMN_DATE)
        {
            return format_date(reply->timestamp, size);
        }
    }
    else
    {
        assert(0);
    }

    return NULL;
}

bool patch_list_is_unread(const struct model_index *index)
{
    const struct buried_comment *comment;
    const struct buried_patch *patch;
    const struct buried_reply *reply;
    const struct buried_rev *rev;
    const struct buried_mr *mr;

    if ((mr = get_mr_from_index(index)))
        return mr->flags & FLAG_UNREAD;
    else if ((rev = get_rev_from_index(index)))
        return rev->flags & FLAG_UNREAD;
    else if ((patch = get_patch_from_index(index)))
        return patch->flags & FLAG_UNREAD;
    else if ((comment = get_comment_from_index(index)))
        return comment->flags & FLAG_UNREAD;
    else if ((reply = get_reply_from_index(index)))
        return reply->flags & FLAG_UNREAD;
    else
        assert(0);

    return NULL;
}

static void mark_read(const char *file, const void *base, uint16_t *flags)
{
    char *path;
    int fd;

    if (!(*flags & FLAG_UNREAD))
        return;

    *flags &= ~FLAG_UNREAD;
    path = get_data_path(active_project, file);
    if ((fd = open(path, O_WRONLY | O_CLOEXEC)) < 0)
        die("Cannot open %s.\n", file);
    pwrite(fd, flags, sizeof(*flags), (const char *)flags - (const char *)base);
    close(fd);
    free(path);
}

void patch_list_mark_read(const struct model_index *index)
{
    struct buried_comment *comment;
    struct buried_patch *patch;
    struct buried_reply *reply;
    struct buried_rev *rev;
    struct buried_mr *mr;

    if ((mr = get_mr_from_index(index)))
        mark_read("mrs0", active_project->mrs, &mr->flags);
    else if ((rev = get_rev_from_index(index)))
        mark_read("revs0", active_project->revs, &rev->flags);
    else if ((patch = get_patch_from_index(index)))
        mark_read("patches0", active_project->patches, &patch->flags);
    else if ((comment = get_comment_from_index(index)))
        mark_read("comments0", active_project->comments, &comment->flags);
    else if ((reply = get_reply_from_index(index)))
        mark_read("replies0", active_project->replies, &reply->flags);
    else
        assert(0);
}

bool get_mr_comments(const struct model_index *index, uint16_t *comment_count,
        const struct buried_comment **comments, const struct buried_comment **focused_comment,
        const struct buried_reply **focused_reply)
{
    const struct buried_comment *comment;
    const struct buried_reply *reply;
    const struct buried_mr *mr;

    if ((comment = get_comment_from_index(index)))
    {
        if (comment->type != COMMENT_TYPE_COMMENT)
            return false;

        mr = &active_project->mrs[mr_index_from_comment(comment)];

        *focused_comment = comment;
        *focused_reply = NULL;
        *comment_count = mr->comment_count;
        *comments = &active_project->comments[mr->comments_offset];
        return true;
    }
    else if ((reply = get_reply_from_index(index)))
    {
        comment = &active_project->comments[comment_index_from_reply(reply)];
        mr = &active_project->mrs[mr_index_from_comment(comment)];

        *focused_comment = NULL;
        *focused_reply = reply;
        *comment_count = mr->comment_count;
        *comments = &active_project->comments[mr->comments_offset];
        return true;
    }
    else
    {
        return false;
    }
}

const struct buried_user *get_user(uint32_t index)
{
    return &active_project->users[index];
}

const struct buried_reply *get_reply(uint32_t offset)
{
    return &active_project->replies[offset];
}

const char *get_project_name(size_t index)
{
    assert(index < project_count);
    return projects[index].display_name;
}

void switch_to_project(size_t index)
{
    info("Switching to project %zu.\n", index);
    assert(index < project_count);
    active_project = &projects[index];
}

/*
 * Copyright 2024 Elizabeth Figura
 *
 * This file is part of Labrador.
 *
 * Labrador is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Labrador is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Labrador; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

enum comment_type
{
    COMMENT_TYPE_COMMENT = 1,
    COMMENT_TYPE_APPROVE = 2,
    COMMENT_TYPE_UNAPPROVE = 3,
    COMMENT_TYPE_CLOSE = 4,
    COMMENT_TYPE_REOPEN = 5,
    COMMENT_TYPE_MERGE = 6,
};

struct buried_patch
{
    uint16_t flags;
    uint16_t message_len;
    uint16_t subject_len;
    uint16_t author_index;
    uint32_t diff_len;
    uint32_t message_offset;
    uint32_t diff_offset;
};

struct buried_comment
{
    /* Same as the note ID but copied here for convenience (and since we have
     * padding here anyway... */
    uint32_t id;
    uint16_t flags;
    uint16_t type;
    union
    {
        /* For comments we store the timestamp, author, and the content of the
         * initial note as a reply. */
        struct
        {
            uint16_t reply_count;
            /* Patch index, if this is for a patch [or -1 if not?] */
            uint16_t patch_offset;
            uint32_t replies_offset;
            /* Absolute line number relative to the stored plaintext diff. */
            uint32_t diff_line_number;
            uint32_t padding;
        } comment;
        struct
        {
            /* Other activity has no reply, so we overload the same fields and
             * store them inline. */
            uint64_t timestamp;
            uint32_t user_index;
            uint32_t padding;
        } activity;
    } u;
};

/* "reply" is a bit misleading since the first comment in a chain is also
 * stored as a reply.
 * Gitlab calls this a "note", but I don't particularly like that either.
 * I live in hope that someday they will grow support for proper threading. */
struct buried_reply
{
    uint64_t timestamp;
    uint16_t flags;
    uint16_t padding;
    uint32_t content_len;
    uint32_t user_index;
    uint32_t content_offset;
    uint32_t id;
    uint32_t padding2;
};

struct buried_user
{
    uint16_t name_len;
    uint16_t username_len;
    uint32_t offset;
};

void labrador_init(void);
void fetch(void);

/* We pull all the logic possible out of C++ and into C, so this is the
 * interop structure for QModelIndex. */
struct model_index
{
    int row, column;
    void *internal;
};

enum patch_list_column
{
    PATCH_LIST_COLUMN_NAME,
    PATCH_LIST_COLUMN_AUTHOR,
    PATCH_LIST_COLUMN_DATE,
    PATCH_LIST_COLUMN_COUNT,
};

int patch_list_get_row_count(const struct model_index *parent);
void patch_list_get_index(struct model_index *index, int row, int column, const struct model_index *parent);
bool patch_list_get_parent(struct model_index *parent, const struct model_index *index);
char *patch_list_get_text(const struct model_index *index, size_t *size);
bool patch_list_is_unread(const struct model_index *index);
void patch_list_mark_read(const struct model_index *index);

struct buried_patch *get_patch_from_index(const struct model_index *index);
bool get_mr_comments(const struct model_index *index, uint16_t *comment_count,
        const struct buried_comment **comments, const struct buried_comment **focused_comment,
        const struct buried_reply **focused_reply);

char *format_date(uint64_t timestamp, size_t *ret_size);
char *dig_string(size_t offset, size_t len);

const struct buried_user *get_user(uint32_t index);
const struct buried_reply *get_reply(uint32_t offset);

extern size_t project_count;
const char *get_project_name(size_t index);
void switch_to_project(size_t index);

#ifdef __cplusplus
}
#endif

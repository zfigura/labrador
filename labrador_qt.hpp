/*
 * Copyright 2024 Elizabeth Figura
 *
 * This file is part of Labrador.
 *
 * Labrador is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Labrador is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Labrador; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <QAbstractItemModel>
#include <QItemSelection>
#include <QModelIndex>
#include <QVariant>

class PatchListModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit PatchListModel(QObject *parent);
    ~PatchListModel();

    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent) const override;

public slots:
    /* I'm sure those OOP folks don't want us to put the slot here, but rather
     * go to the work of subclassing something else or inventing our own class.
     * Fie on them. */
    void selection_changed_slot(const QItemSelection &selected, const QItemSelection &deselected);

    /* Yes, I'm even reusing this for unrelated objects. Don't care!
     * Hey, if Qt let me use non-object callbacks I would.
     * But as it is I'm not going to the trouble of writing more class
     * boilerplate. */
    void project_changed_slot(void);
};

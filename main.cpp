/*
 * Copyright 2024 Elizabeth Figura
 *
 * This file is part of Labrador.
 *
 * Labrador is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Labrador is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Labrador; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <QAbstractItemModel>
#include <QAbstractItemView>
#include <QApplication>
#include <QFontDatabase>
#include <QFrame>
#include <QLabel>
#include <QListWidget>
#include <QMainWindow>
#include <QModelIndex>
#include <QScrollArea>
#include <QSplitter>
#include <QString>
#include <QTextEdit>
#include <QTreeView>
#include <QVariant>
#include <QVBoxLayout>
#include <QWidget>

#include "labrador.h"
#include "labrador_qt.hpp"

static void model_index_to_c(struct model_index *dst, const QModelIndex &src)
{
    dst->row = src.row();
    dst->column = src.column();
    dst->internal = src.internalPointer();
}

PatchListModel::PatchListModel(QObject *parent) : QAbstractItemModel(parent)
{
}

PatchListModel::~PatchListModel()
{
}

int PatchListModel::columnCount(const QModelIndex &parent) const
{
    return PATCH_LIST_COLUMN_COUNT;
}

int PatchListModel::rowCount(const QModelIndex &parent) const
{
    struct model_index c_parent;

    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        return patch_list_get_row_count(NULL);

    model_index_to_c(&c_parent, parent);
    return patch_list_get_row_count(&c_parent);
}

QModelIndex PatchListModel::index(int row, int column, const QModelIndex &parent) const
{
    struct model_index c_index, c_parent;
    bool ret;

    if (!parent.isValid())
    {
        patch_list_get_index(&c_index, row, column, NULL);
    }
    else
    {
        model_index_to_c(&c_parent, parent);
        patch_list_get_index(&c_index, row, column, &c_parent);
    }
    return createIndex(c_index.row, c_index.column, c_index.internal);
}

QModelIndex PatchListModel::parent(const QModelIndex &index) const
{
    struct model_index c_index, c_parent;
    model_index_to_c(&c_index, index);
    if (!patch_list_get_parent(&c_parent, &c_index))
        return QModelIndex();
    return createIndex(c_parent.row, c_parent.column, c_parent.internal);
}

QVariant PatchListModel::data(const QModelIndex &index, int role) const
{
    struct model_index c_index;
    QVariant variant;

    model_index_to_c(&c_index, index);

    if (role == Qt::DisplayRole)
    {
        size_t size;
        char *ret;

        if (!(ret = patch_list_get_text(&c_index, &size)))
            return QVariant();
        variant = QVariant(QString::fromUtf8(ret, size));
        free(ret);
        return variant;
    }
    else if (role == Qt::FontRole)
    {
        QFont font;
        if (patch_list_is_unread(&c_index))
            font.setBold(true);
        return font;
    }
    else
    {
        return QVariant();
    }
}

static QWidget *body_pane;
static QVBoxLayout *body_pane_layout;
static QTextEdit *body_text;
static QWidget *comments_box;
static QVBoxLayout *comments_box_layout;
static QScrollArea *comments_scroll;
static QListWidget *project_list;

static QString dig_qstring(uint32_t offset, uint32_t len)
{
    char *s = dig_string(offset, len);
    QString ret = QString::fromUtf8(s, len);
    free(s);
    return ret;
}

static QString format_date_as_qstring(uint64_t timestamp)
{
    size_t size;
    char *s = format_date(timestamp, &size);
    QString ret = QString::fromUtf8(s, size);
    free(s);
    return ret;
}

static QString format_user(uint32_t index)
{
    const struct buried_user *user = get_user(index);
    char *name = dig_string(user->offset, user->name_len + user->username_len);
    QString ret;
    size_t size;
    char *s;

    size = asprintf(&s, "%.*s (@%.*s)", user->name_len, name,
            user->username_len, name + user->name_len);
    ret = QString::fromUtf8(s, size);
    free(s);
    return ret;
}

void PatchListModel::selection_changed_slot(const QItemSelection &selected,
        const QItemSelection &deselected)
{
    const struct buried_comment *focused_comment, *comments;
    QModelIndex index = selected.indexes().first();
    const struct buried_reply *focused_reply;
    const struct buried_patch *patch;
    struct model_index c_index;
    uint16_t comment_count;
    size_t size;
    char *ret;

    while (comments_box_layout->count())
    {
        QWidget *widget = comments_box_layout->itemAt(0)->widget();
        comments_box_layout->removeWidget(widget);
        delete widget;
    }

    model_index_to_c(&c_index, index);

    patch_list_mark_read(&c_index);

    if ((patch = get_patch_from_index(&c_index)))
    {
        body_text->setPlainText(dig_qstring(patch->diff_offset, patch->diff_len));

        body_text->show();
        comments_scroll->hide();
    }
    else if (get_mr_comments(&c_index, &comment_count, &comments, &focused_comment, &focused_reply))
    {
        QWidget *focus = NULL;

        /* Comments are a bit more complicated than patches.
         *
         * Showing the text of whatever comment or reply is selected, isn't
         * hard. However, Gitlab's default UI shows all the comments right next
         * to each other, and people therefore often do away with
         * quote-replying. This means that in order to make replies readable we
         * need to do the same. Hence we render all replies at once, and scroll
         * to and highlight the one that's been selected in the patch list.
         *
         * On top of that: Although Gitlab's intent was probably for each
         * top-level comment to be an independent discussion thread, in
         * practice people often reply to comments using new top-level
         * comments. This is probably a mix of users deciding they don't want
         * to deal with the halfhearted attempt at threading and treating the
         * platform like a forum / bulletin board, and the fact that in the
         * default UI it's more convenient to create a new top-level comment
         * than to create a reply.
         *
         * This isn't always done, but it means that necessary context for a
         * comment may include not just all the other replies in a chain, but
         * also all the comments on the entire merge request. */

        body_text->hide();
        comments_scroll->show();

        for (uint16_t i = 0; i < comment_count; ++i)
        {
            const struct buried_comment *comment = &comments[i];

            if (comment->type == COMMENT_TYPE_COMMENT)
            {
                const struct buried_reply *reply = get_reply(comment->u.comment.replies_offset);

                QFrame *box = new QFrame;
                QVBoxLayout *layout = new QVBoxLayout(box);
                QLabel *author = new QLabel(format_user(reply->user_index));
                QLabel *date = new QLabel(format_date_as_qstring(reply->timestamp));
                QLabel *text = new QLabel(dig_qstring(reply->content_offset, reply->content_len));

                text->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));

                QWidget *header = new QWidget;
                QHBoxLayout *header_layout = new QHBoxLayout(header);

                header_layout->addWidget(author);
                header_layout->addWidget(date);

                layout->addWidget(header);
                layout->addWidget(text);

                comments_box_layout->addWidget(box);
                box->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
                box->show();

                if (focused_comment == comment)
                    focus = box;

                for (uint16_t j = 1; j < comment->u.comment.reply_count; ++j)
                {
                    reply = get_reply(comment->u.comment.replies_offset + j);

                    QFrame *reply_box = new QFrame;
                    QVBoxLayout *reply_layout = new QVBoxLayout(reply_box);
                    QLabel *author = new QLabel(format_user(reply->user_index));
                    QLabel *date = new QLabel(format_date_as_qstring(reply->timestamp));
                    QLabel *text = new QLabel(dig_qstring(reply->content_offset, reply->content_len));

                    text->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));

                    QWidget *header = new QWidget;
                    QHBoxLayout *header_layout = new QHBoxLayout(header);

                    header_layout->addWidget(author);
                    header_layout->addWidget(date);

                    reply_layout->addWidget(header);
                    reply_layout->addWidget(text);

                    layout->addWidget(reply_box);
                    reply_box->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
                    reply_box->show();

                    if (focused_reply == reply)
                        focus = reply_box;
                }
            }
            else
            {
                /* TODO */
            }
        }

        focus->setAutoFillBackground(true);
        focus->setBackgroundRole(QPalette::Highlight);
        comments_scroll->ensureWidgetVisible(focus);
    }
    else
    {
        body_text->hide();
    }

    // todo: patch comments
}

void PatchListModel::project_changed_slot(void)
{
    // fixme: this is not thread safe!
    this->beginResetModel();
    switch_to_project(project_list->currentRow());
    this->endResetModel();
}

int main(int argc, char **argv)
{
    QSplitter *splitter1, *splitter2;
    QTreeView *patch_list;
    QMainWindow *win;

    QWidget *left_pane;
    QVBoxLayout *left_pane_layout;
    QLabel *project_list_label;

    labrador_init();

    /* This is temporary. Eventually fetching will be made into a background
     * task and integrated into the UI, but we're not there yet. */
    if (argc == 2 && !strcmp(argv[1], "fetch"))
    {
        fetch();
        return 0;
    }

    QApplication app(argc, argv);
    PatchListModel patch_list_model(nullptr);

    win = new QMainWindow();
    win->setWindowTitle("Labrador");
    win->resize(1200, 800);

    splitter1 = new QSplitter;
    splitter1->setOrientation(Qt::Horizontal);

    /* FIXME: gettext */
    project_list_label = new QLabel("Projects");
    project_list = new QListWidget;
    project_list->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);

    /* Populate the project list now, before initializing the patch list,
     * so that we already have the active project set. */
    for (size_t i = 0; i < project_count; ++i)
        new QListWidgetItem(get_project_name(i), project_list);
    project_list->setCurrentRow(0);
    switch_to_project(0);

    QObject::connect(project_list, &QListWidget::itemSelectionChanged,
            &patch_list_model, &PatchListModel::project_changed_slot);

    left_pane = new QWidget;
    left_pane_layout = new QVBoxLayout(left_pane);
    left_pane_layout->addWidget(project_list_label);
    left_pane_layout->addWidget(project_list);

    splitter2 = new QSplitter;
    splitter2->setOrientation(Qt::Vertical);

    patch_list = new QTreeView;
    patch_list->setUniformRowHeights(true);
    patch_list->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
    /* In theory we'd use setSelectionBehavior(SelectRows) here, but...
     * apparently the code already does what we want? Confusing.
     *
     * There's also setAllColumnsShowFocus(), which is under-documented. */
    patch_list->setModel(&patch_list_model);

    QObject::connect(patch_list->selectionModel(), &QItemSelectionModel::selectionChanged,
            &patch_list_model, &PatchListModel::selection_changed_slot);

    body_pane = new QWidget;
    body_pane_layout = new QVBoxLayout(body_pane);

    body_text = new QTextEdit;
    body_text->setReadOnly(true);
    body_text->setCurrentFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
    body_pane_layout->addWidget(body_text);
    body_text->hide();

    comments_scroll = new QScrollArea;
    comments_box = new QWidget;
    comments_box_layout = new QVBoxLayout(comments_box);
    comments_scroll->setWidget(comments_box);
    body_pane_layout->addWidget(comments_scroll);
    comments_scroll->hide();

    /* We want the scroll area contents to be at least as large as the scroll
     * area itself; setWidgetResizable(true) does this. */
    comments_scroll->setWidgetResizable(true);
    /* However, we don't want the contents to stretch vertically if they're
     * smaller than the scroll area. Specify that the "preferred" vertical size
     * (i.e. the default sizeHint) is also the maximum, and the widget should
     * never be stretched. */
    comments_box->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

    /* Splitter stretch factors are, for some odd reason, multiplied by the
     * size of the widget before applying, which is a little undesirable.
     * setSizes() accepts pixels, which isn't really ideal either.
     *
     * Fortunately, setSizes() also will scale all sizes to the size of the
     * widget in case of overflow or underflow, so we can just specify the
     * stretch factor there. However, it does this *after* clamping to the
     * widget's minimum size, so we need to use a "large enough" number.
     *
     * This is ugly, but ideally we'll eventually have the user's layout saved
     * and restored, so it won't matter. */
    splitter1->addWidget(left_pane);
    splitter1->addWidget(splitter2);
    splitter1->setSizes({1000, 9000});

    splitter2->addWidget(patch_list);
    splitter2->addWidget(body_pane);
    splitter2->setSizes({4000, 6000});

    win->setCentralWidget(splitter1);

    /* Needless to say, this is a hack. */
    patch_list->setColumnWidth(PATCH_LIST_COLUMN_NAME, 680);
    patch_list->setColumnWidth(PATCH_LIST_COLUMN_AUTHOR, 140);

    win->show();

    return app.exec();
}
